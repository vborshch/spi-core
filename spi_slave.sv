
//
// Project       : SPI core
// Author        : Borchsh Vladislav
// Contacts      : borchsh.vn@gmail.com
// Workfile      : spi_slave.sv
// Description   : SPI slave module
//

`ifndef __SPI_SLAVE__
`define __SPI_SLAVE__

module spi_slave
  #(
    parameter int pW = 32 // One packet size
  )
  (
    input                     iclk   ,
    input                     irst   ,
    // SPI PHY
    input                     isclk  ,
    input                     imosi  ,
    input                     ics_n  ,
    output logic              omiso  ,
    // Data interface
    input                     isop   ,
    input            [pW-1:0] idat   ,
    output logic              osop   , // start of packet
    output logic              oeob   , // end of one byte signal
    output logic              oeop   , // end of packet
    output logic              oerr   , // not full packet
    output logic     [pW-1:0] odat
    
  );

  //--------------------------------------------------------------------------------------------------------
  // Declaration variables
  //--------------------------------------------------------------------------------------------------------

  logic              [2:0] fmosi;
  logic              [2:0] fcs_n;
  logic              [2:0] fsclk;
  logic                    true_sclk;
  logic                    true_mosi;
  logic                    true_cs_n;

  logic                    raise_sclk;
  logic                    fall_sclk;
  logic                    fall_cs_n;
  logic                    raise_cs_n;

  logic   [$clog2(pW)+1:0] cnt_dat;
  logic           [pW-1:0] rx_shift_reg = '0;
  logic           [pW-1:0] tx_shift_reg = '0;

  //--------------------------------------------------------------------------------------------------------
  // BODY
  //--------------------------------------------------------------------------------------------------------

  // Debouncing & metastable protect
  always_ff@(posedge iclk) begin
    fsclk <= fsclk << 1 | isclk;
    fmosi <= fmosi << 1 | imosi;
    fcs_n <= fcs_n << 1 | ics_n;

    true_sclk <= &fsclk[2:1];
    true_mosi <= &fmosi[2:1];
    true_cs_n <= &fcs_n[2:1];
  end

  // Front detections
  always_ff@(posedge iclk) begin
    raise_sclk <= (~true_sclk) & ( &fsclk[2:1]);
    fall_sclk  <= ( true_sclk) & (~&fsclk[2:1]);
    fall_cs_n  <= ( true_cs_n) & ( ~fcs_n[2:1]);
    raise_cs_n <= (~true_cs_n) & ( &fcs_n[2:1]);
  end

  // Data collect
  always_ff@(posedge iclk) begin
    if (irst | fall_cs_n)              cnt_dat <= '0;
    else if (raise_sclk & ~&cnt_dat)   cnt_dat <= cnt_dat + 1'b1;

    if (irst)                          rx_shift_reg <= '0;
    else if (~true_cs_n & raise_sclk)  rx_shift_reg <= rx_shift_reg << 1 | true_mosi;
  end

  // Response to master
  always_ff@(posedge iclk) begin
    if (irst)               tx_shift_reg <= idat;
    else if (isop)          tx_shift_reg <= idat;
      else if (raise_sclk)  tx_shift_reg <= tx_shift_reg << 1 | 1'b0;
  end

  //--------------------------------------------------------------------------------------------------
  // Output signals
  //--------------------------------------------------------------------------------------------------

  always_ff@(posedge iclk) begin
    osop  <= fall_cs_n;
    odat  <= rx_shift_reg;
    oeop  <= raise_cs_n & (cnt_dat == pW);
    oerr  <= raise_cs_n & (cnt_dat != pW);
    oeob  <= (cnt_dat%8 == 'd0) & fall_sclk;
    omiso <= tx_shift_reg[$high(tx_shift_reg)];
  end

endmodule

`endif
